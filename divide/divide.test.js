const divide = require("./divide")

describe("divide", () => {
  it("Should divide dividend by divisor", () => {
    expect(divide(0.1, 0.2)).toEqual(0.5)
    expect(divide(0.1)(0.2)).toEqual(0.5)
    expect(divide("0.1")("0.2")).toEqual(0.5)
    expect(divide(-0.1, -0.2)).toEqual(0.5)
    expect(divide(-0.1)(-0.2)).toEqual(0.5)
    expect(divide(-0.1, 0.2)).toEqual(-0.5)
    expect(divide(-0.1)(0.2)).toEqual(-0.5)
    expect(divide(-0.01, 0.02)).toEqual(-0.5)
    expect(divide(-0.01)(-0.2)).toEqual(0.05)
  })
})
