const Decimal = require("decimal.js-light")

const divide = (dividend, divisor) => Number(new Decimal(dividend).dividedBy(divisor))

module.exports = (dividend, divisor) => {
  if (divisor === undefined) {
    return n => divide(dividend, n)
  }

  return divide(dividend, divisor)
}
