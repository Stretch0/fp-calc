const Decimal = require("decimal.js-light")

const multiply = (multiplicand, multiplier) => Number(new Decimal(multiplicand).times(multiplier))

module.exports = (multiplicand, multiplier) => {
  if (multiplier === undefined) {
    return n => multiply(multiplicand, n)
  }

  return multiply(multiplicand, multiplier)
}
