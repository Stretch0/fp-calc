const multiply = require("./multiply")

describe("multiply", () => {
  it("Should calculate the multiply of an array of numbers without rounding errors", () => {
    expect(multiply(0.1, 0.2)).toEqual(0.02)
    expect(multiply(0.1)(0.2)).toEqual(0.02)
    expect(multiply("0.1")("0.2")).toEqual(0.02)
    expect(multiply(-0.1, -0.2)).toEqual(0.02)
    expect(multiply(-0.1)(-0.2)).toEqual(0.02)
    expect(multiply(-0.1, 0.2)).toEqual(-0.02)
    expect(multiply(-0.1)(0.2)).toEqual(-0.02)
    expect(multiply(-0.01, 0.02)).toEqual(-0.0002)
    expect(multiply(-0.01)(-0.2)).toEqual(0.002)
  })
})
