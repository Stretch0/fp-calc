const Decimal = require("decimal.js-light")

const subtract = (minuend, subtrahend) => Number(new Decimal(minuend).minus(subtrahend))

module.exports = (minuend, subtrahend) => {
  if (subtrahend === undefined) {
    return n => subtract(minuend, n)
  }

  return subtract(minuend, subtrahend)
}
