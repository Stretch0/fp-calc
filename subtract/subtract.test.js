const subtract = require("./subtract")

describe("subtract", () => {
  it("Should the second argument from the first argument", () => {
    expect(subtract(0.1, 0.2)).toEqual(-0.1)
    expect(subtract(0.1)(0.2)).toEqual(-0.1)
    expect(subtract(-0.1, -0.2)).toEqual(0.1)
    expect(subtract(-0.1)(-0.2)).toEqual(0.1)
    expect(subtract(-0.1, 0.2)).toEqual(-0.3)
    expect(subtract(-0.1)(0.2)).toEqual(-0.3)
    expect(subtract(1.33, 1.32)).toEqual(0.01)
    expect(subtract(1.33)(1.32)).toEqual(0.01)
    expect(subtract(1.33, 1.14)).toEqual(0.19)
    expect(subtract(1.33)(1.14)).toEqual(0.19)
  })
})
