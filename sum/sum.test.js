const sum = require("./sum")

describe("sum", () => {
  it("Should calculate the sum of an array of numbers without rounding errors", () => {
    expect(sum([0.1, 0.2])).toEqual(0.3)
    expect(sum([-0.1, -0.2])).toEqual(-0.3)
    expect(sum([-0.1, 0.2])).toEqual(0.1)
    expect(sum([100.01, 100.02, 100.03, 100.4])).toEqual(400.46)
  })
})
