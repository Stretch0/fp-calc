const Decimal = require("decimal.js-light")

const add = (total, n) => {
  return total.plus(n)
}

module.exports = numbers => Number(numbers.reduce(add, new Decimal(0)))
